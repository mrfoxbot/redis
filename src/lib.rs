#![feature(
    async_await,
    await_macro,
    decl_macro,
    futures_api,
    try_trait,
    try_from,
)]

#[macro_use] extern crate log;
#[macro_use] extern crate redis_async;
#[macro_use] extern crate serde;


pub mod model;

mod cache;
mod cmds;
mod error;
mod gen;
mod resp_impl;

pub use crate::{
    cache::Cache,
    error::{Error, Result},
};