use crate::{
    error::Error as CacheError,
    resp_impl::RespValueExt,
};
use redis_async::{
    error::Error as RedisError,
    resp::{FromResp, RespValue},
};
use serde::de::DeserializeOwned;
use serde_aux::prelude::*;
use serde_json::{Map, Number, Value};
use std::{
    collections::HashSet,
    convert::TryFrom,
};

fn convert<T: DeserializeOwned>(resp: RespValue) -> Result<T, RedisError> {
    let values = match resp {
        RespValue::Array(x) => x,
        _ => return Err(RedisError::RESP("Expected an array".to_owned(), None)),
    };

    let map = create_hashmap(values);

    // Should this really not panic? Is it better to let the user handle the error, or should we
    // force unwinds for them to realise what happened?
    //
    // Ok(serde_json::from_value(Value::from(map)).expect("err deserializing"))

    match serde_json::from_value(Value::from(map)) {
        Ok(deserialized) => Ok(deserialized),
        Err(err) => Err(RedisError::Unexpected(format!("Couldn't deserialize a cached value: err={:?}", err))),
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Guild {
    pub afk_channel_id: Option<u64>,
    pub channels: HashSet<u64>,
    pub features: HashSet<String>,
    pub members: HashSet<u64>,
    pub name: String,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub owner_id: u64,
    pub region: String,
    pub roles: HashSet<u64>,
    pub voice_states: HashSet<u64>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GuildChannel {
    pub bitrate: Option<u64>,
    pub category_id: Option<u64>,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub kind: u64,
    pub name: String,
    pub permission_overwrites: Vec<PermissionOverwrite>,
    pub user_limit: Option<u64>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Member {
    pub deaf: bool,
    pub nick: Option<String>,
    pub roles: Vec<u64>,
    pub user: User,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PermissionOverwrite {
    pub allow: u64,
    pub deny: u64,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub kind: u64,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Role {
    #[serde(deserialize_with = "deserialize_string_from_number")]
    pub name: String,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub permissions: u64,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct User {
    pub bot: bool,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub discriminator: u16,
    #[serde(deserialize_with = "deserialize_number_from_string")]
    pub id: u64,
    pub name: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct VoiceState {
    pub channel_id: Option<u64>,
    pub endpoint: Option<String>,
    pub mute: Option<bool>,
    pub self_deaf: Option<bool>,
    pub self_mute: Option<bool>,
    pub session_id: Option<String>,
    pub suppress: Option<bool>,
    pub token: Option<String>,
}

impl FromResp for VoiceState {
    fn from_resp_int(resp: RespValue) -> Result<Self, RedisError> {

        let values = match resp {
            RespValue::Array(x) => x,
            _ => return Err(RedisError::RESP("Expected an array".to_owned(), None)),
        };

        let map = create_hashmap(values);

        let mut channel_id = None;
        let mut endpoint = None;
        let mut mute = None;
        let mut self_deaf = None;
        let mut self_mute = None;
        let mut session_id = None;
        let mut suppress = None;
        let mut token = None;

        for (key, value) in map.into_iter() {
            match &*key {
                "channel_id" => channel_id = to_u64(value),
                "endpoint" => endpoint = to_str(value),
                "mute" => mute = to_bool(value),
                "self_deaf" => self_deaf = to_bool(value),
                "self_mute" => self_mute = to_bool(value),
                "session_id" => session_id = to_str(value),
                "suppress" => suppress = to_bool(value),
                "token" => token = to_str(value),
                other => {
                    warn!("Unknown voice state key: {:?}", other);
                },
            }
        }

        Ok(Self {
            channel_id,
            endpoint,
            mute,
            self_deaf,
            self_mute,
            session_id,
            suppress,
            token,
        })
    }
}

fn to_bool(value: Value) -> Option<bool> {
    match value {
        Value::Bool(boolean) => Some(boolean),
        Value::String(string) => match &*string {
            "0" => Some(false),
            "1" => Some(true),
            other => {
                warn!("Can't convert {:?} into a bool", other);

                None
            },
        },
        other => {
            warn!("Not a u64: {:?}", other);

            None
        },
    }
}

fn to_u64(value: Value) -> Option<u64> {
    match value {
        Value::Number(num) => num.as_u64(),
        Value::String(num) => num.parse().ok(),
        other => {
            warn!("Not a u64: {:?}", other);

            None
        },
    }
}

fn to_str(value: Value) -> Option<String> {
    match value {
        Value::Number(num) => Some(format!("{}", num)),
        Value::String(string) => Some(string),
        other => {
            warn!("Not a str: {:?}", other);

            None
        },
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub enum LoopMode {
    Queue,
    Song,
    Off,
}

impl LoopMode {
    const LOOPING_QUEUE_ENCODED: &'static str = "LQ";
    const LOOPING_SONG_ENCODED: &'static str  = "LS";
    const LOOPING_OFF_ENCODED: &'static str   = "OF";
}

impl Into<String> for LoopMode {
    // From cannot fail, while deserializing can
    // Into cannot fail, which serializing cannot, meaning this fits

    fn into(self) -> String {
        match self {
            LoopMode::Queue => String::from(Self::LOOPING_QUEUE_ENCODED),
            LoopMode::Song => String::from(Self::LOOPING_SONG_ENCODED),
            LoopMode::Off => String::from(Self::LOOPING_OFF_ENCODED),
        }
    }
}

impl TryFrom<String> for LoopMode {
    type Error = CacheError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        match &*value {
            Self::LOOPING_QUEUE_ENCODED => Ok(LoopMode::Queue),
            Self::LOOPING_SONG_ENCODED => Ok(LoopMode::Song),
            Self::LOOPING_OFF_ENCODED => Ok(LoopMode::Off),
            _ => Err(CacheError::InvalidLoopMode),
        }
    }
}

fn create_hashmap(resp: Vec<RespValue>) -> Map<String, Value> {
    let mut map = Map::with_capacity(resp.len() / 2);
    let mut iter = resp.into_iter();

    loop {
        let key = match iter.next() {
            Some(key) => key,
            None => break,
        };
        let value = iter.next().unwrap();
        let v = resp_to_value(value);
        map.insert(key.into_string(), v);
    }

    map
}

fn resp_to_value(resp: RespValue) -> Value {
    match resp {
        RespValue::Nil => Value::Null,
        RespValue::Array(resps) => Value::Array(resps.into_iter().map(resp_to_value).collect()),
        RespValue::BulkString(bytes) => {
            let string = String::from_utf8(bytes).unwrap();

            Value::String(string)
        },
        RespValue::Error(why) => panic!("{:?}", why),
        RespValue::Integer(integer) => Value::Number(Number::from(integer)),
        RespValue::SimpleString(string) => Value::String(string),
    }
}


macro from_resp_impls($($struct:ident,)+) {
    $(
        impl FromResp for $struct {
            fn from_resp_int(resp: RespValue) -> Result<Self, RedisError> {
                convert(resp)
            }
        }
    )+
}

from_resp_impls![
    Guild,
    GuildChannel,
    Member,
    PermissionOverwrite,
    Role,
    User,
];

#[cfg(test)]
mod tests {
    use crate::error::Result;
    use redis_async::resp::{FromResp, RespValue};
    use super::*;

    #[test]
    fn test_role() -> Result<()> {
        let value = RespValue::Array(vec![
            RespValue::BulkString(b"name".to_vec()),
            RespValue::BulkString(b"test".to_vec()),
            RespValue::BulkString(b"permissions".to_vec()),
            RespValue::BulkString(b"8".to_vec()),
        ]);

        Role::from_resp(value)?;

        let value = RespValue::Array(vec![
            RespValue::BulkString(b"name".to_vec()),
            RespValue::BulkString(b"0123456".to_vec()),
            RespValue::BulkString(b"permissions".to_vec()),
            RespValue::BulkString(b"8".to_vec()),
        ]);

        Role::from_resp(value)?;

        Ok(())
    }

    #[test]
    fn test_voice_state() {
        let value = RespValue::Array(vec![
            RespValue::BulkString(b"channel_id".to_vec()),
            RespValue::BulkString(b"500000000000000000".to_vec()),
            RespValue::BulkString(b"session_id".to_vec()),
            RespValue::BulkString(b"946f395aa3c194fda2aa7baa2e402d2b".to_vec()),
            RespValue::BulkString(b"token".to_vec()),
            RespValue::BulkString(b"450d2eedffbdad13".to_vec()),
        ]);

        assert!(VoiceState::from_resp(value).is_ok());
    }

    #[test]
    fn test_voice_state_numeric_fields() -> Result<()> {
        VoiceState::from_resp(RespValue::Array(vec![
            RespValue::BulkString(b"channel_id".to_vec()),
            RespValue::BulkString(b"500000000000000000".to_vec()),
            RespValue::BulkString(b"session_id".to_vec()),
            RespValue::BulkString(b"946f395aa3c194fda2aa7baa2e402d2b".to_vec()),
        ]))?;

        VoiceState::from_resp(RespValue::Array(vec![
            RespValue::BulkString(b"channel_id".to_vec()),
            RespValue::BulkString(b"500000000000000000".to_vec()),
            RespValue::BulkString(b"session_id".to_vec()),
            RespValue::BulkString(b"11111111111111111111111111111111".to_vec()),
        ]))?;

        Ok(())
    }

    #[test]
    fn test_loop_mode() {
        let value = String::from(LoopMode::LOOPING_QUEUE_ENCODED);
        assert!(LoopMode::try_from(value).is_ok());

        let value = String::from(LoopMode::LOOPING_SONG_ENCODED);
        assert!(LoopMode::try_from(value).is_ok());

        let value = String::from(LoopMode::LOOPING_OFF_ENCODED);
        assert!(LoopMode::try_from(value).is_ok());

        let value = String::from("error me pls");
        let value = LoopMode::try_from(value);
        match value {
            Ok(_) => panic!("didn't error"),
            Err(_) => {},
        }
    }
}